# Web Dev General Mailing List

Intended to send out news whenever somebody updates this repository.

## Usage

The repository is configured to post news whenever there's a change in master branch.
In addition, master branch cannot have anything pushed to it directly. Instead
things can only appear in it via merge requests. This is to prevent accidental 
spam.

To post news onto the channel you must rewrite the contents of `news.txt`
file. The entire file will be posted to webhook configured in the pipelines.

Merge requests are only accepted if pipeline passes. Actual deployment happens
when someone agrees to run the `deploy_production` job.

## Intended workflow

Anyone who wants to post news should first make merge request on
`additional_reports` branch. Someone will come along to review the request
and either merge it or decline it. Then after some time `additional_reports` branch
is merged into `master` and then is posted to main `info` channel.
